import React, { Component } from 'react'
import BarraNav from './components/barra-nav/BarraNav'
import Container from 'react-bootstrap/Container'
import Nav from 'react-bootstrap/Nav'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Tab from 'react-bootstrap/Tab'

export default class EjIntermedios extends Component {
    render() {
        return (
            <React.Fragment>
              <BarraNav />
              <Container>
                <h1 className="text-center text-primary font-weight-bold p-3">Ejercicios Intermedios</h1>
                <Tab.Container id="left-tabs-example" defaultActiveKey="first">
                  <Row>
                    <Col sm={3}>
                      <Nav variant="pills" className="flex-column">
                        <Nav.Item>
                          <Nav.Link eventKey="first">Ejercicio 1</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                          <Nav.Link eventKey="second">Ejercicio 2</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                          <Nav.Link eventKey="third">Ejercicio 3</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                          <Nav.Link eventKey="fourth">Ejercicio 4</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                          <Nav.Link eventKey="fifth">Ejercicio 5</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                          <Nav.Link eventKey="sixth">Ejercicio 6</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                          <Nav.Link eventKey="seventh">Ejercicio 7</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                          <Nav.Link eventKey="eighth">Ejercicio 8</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                          <Nav.Link eventKey="nineth">Ejercicio 9</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                          <Nav.Link eventKey="tenth">Ejercicio 10</Nav.Link>
                        </Nav.Item>
                      </Nav>
                    </Col>
                    <Col sm={9}>
                      <Tab.Content>
                        <Tab.Pane eventKey="first">
                          <p>Escriba un programa que genere todas las posibilidades de poner + o - o nada entre los números 1,2, ..., 9 (en este orden) de manera que el resultado sea 100. Por ejemplo 1 + 2 + 3 - 4 + 5 + 6 + 78 + 9 = 100.</p>
                        </Tab.Pane>
                        <Tab.Pane eventKey="second">
                          <p>Escriba un programa que tome la duración de un año (en días fraccionarios) para un planeta imaginario como información y produzca una regla de año bisiesto que minimice la diferencia con respecto al año solar del planeta.</p>
                        </Tab.Pane>
                        <Tab.Pane eventKey="third">
                          <p>Implementar una estructura de datos para gráficos que permita la modificación (inserción, eliminación). Debería ser posible almacenar valores en bordes y nodos. Podría ser más fácil usar un diccionario de (nodo, lista) para hacer esto.</p>
                        </Tab.Pane>
                        <Tab.Pane eventKey="fourth">
                          <p>Escribe una función que genere una representación DOT de un gráfico.</p>
                        </Tab.Pane>
                        <Tab.Pane eventKey="fifth">
                          <p>Modifique el programa anterior de modo que solo se consideren múltiplos de tres en la suma, p. Ej. 3, 6, 9, 12, 15, ...</p>
                        </Tab.Pane>
                        <Tab.Pane eventKey="sixth">
                          <p>Modifique el programa anterior de modo que solo se consideren múltiplos de tres o cinco en la suma, p. Ej. 3, 5, 6, 9, 10, 12, 15, ...</p>
                        </Tab.Pane>
                        <Tab.Pane eventKey="seventh">
                          <p>Escriba un programa que le pida al usuario un número n y le dé la posibilidad de elegir entre calcular la suma y calcular el producto de 1, ..., n.</p>
                        </Tab.Pane>
                        <Tab.Pane eventKey="eighth">
                          <p>Escribe un programa que imprima una tabla de multiplicar para los números hasta 12.</p>
                        </Tab.Pane>
                        <Tab.Pane eventKey="nineth">
                          <p>Escribe un programa que imprima todos los números primos. (Nota: si su lenguaje de programación no admite números de tamaño arbitrario, también está bien imprimir todos los números primos hasta el número más grande que pueda representar fácilmente).</p>
                        </Tab.Pane>
                        <Tab.Pane eventKey="tenth">
                          <p>Escribe un juego de adivinanzas donde el usuario tenga que adivinar un número secreto. Después de cada conjetura, el programa le dice al usuario si su número era demasiado grande o demasiado pequeño. Al final se debe imprimir el número de intentos necesarios. Solo cuenta como un intento si ingresan el mismo número varias veces consecutivas.</p>
                        </Tab.Pane>
                      </Tab.Content>
                    </Col>
                  </Row>
                </Tab.Container>
              </Container>
            </React.Fragment>
        )
    }
}
