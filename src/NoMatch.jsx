import React from 'react'
import notfound from './images/404-error.svg'
const NoMatch = () => {
    return (
        <div className="text-center">
            <h1 className="text-danger p-3">¡Página web no encontrada!</h1>
            <img src={notfound} alt="No encontrado"/>
        </div>
    )
}

export default NoMatch;
