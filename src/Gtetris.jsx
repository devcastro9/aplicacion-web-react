import React, { Component } from 'react'
import Tetris from './components/tetris/components/Tetris'

export default class Gtetris extends Component {
    render() {
        return (
            <React.Fragment>
                <Tetris />
            </React.Fragment>
        )
    }
}