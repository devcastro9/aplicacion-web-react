import React, { Component } from 'react'
import BarraNav from './components/barra-nav/BarraNav'
import Container from 'react-bootstrap/Container'
import Tab from 'react-bootstrap/Tab'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Nav from 'react-bootstrap/Nav'
import Ejer1 from './components/cadena/Ejer1'
/* import Ejer2 from './components/cadena/Ejer2' */

export default class EjCad extends Component {
    render() {
        return (
            <React.Fragment>
              <BarraNav />
              <Container>
                <h1 className="text-center text-primary font-weight-bold p-3">Ejercicios de Listas, Cadenas y Textos.</h1>
                <Tab.Container id="left-tabs-example" defaultActiveKey="first">
                  <Row>
                    <Col sm={3}>
                      <Nav variant="pills" className="flex-column">
                        <Nav.Item>
                          <Nav.Link eventKey="first">Ejercicio 1</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                          <Nav.Link eventKey="second">Ejercicio 2</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                          <Nav.Link eventKey="third">Ejercicio 3</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                          <Nav.Link eventKey="fourth">Ejercicio 4</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                          <Nav.Link eventKey="fifth">Ejercicio 5</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                          <Nav.Link eventKey="sixth">Ejercicio 6</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                          <Nav.Link eventKey="seventh">Ejercicio 7</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                          <Nav.Link eventKey="eighth">Ejercicio 8</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                          <Nav.Link eventKey="nineth">Ejercicio 9</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                          <Nav.Link eventKey="tenth">Ejercicio 10</Nav.Link>
                        </Nav.Item>
                      </Nav>
                    </Col>
                    <Col sm={9}>
                      <Tab.Content>
                        <Tab.Pane eventKey="first">
                          <p>Pedir un numero, y luego mostrar la lista de los multiplos de 3 desde 1 hasta el numero solicitado y luego mostrar la suma de todo los multiplos</p>
                          <Ejer1 />
                        </Tab.Pane>
                        <Tab.Pane eventKey="second">
                          <p>Escribe una función que devuelva el elemento mayor en una lista.</p>
                          {/* <Ejer2 /> */}
                        </Tab.Pane>
                        <Tab.Pane eventKey="third">
                          <p>Escribe una función de escritura que invierte una lista, preferiblemente en su lugar.</p>
                        </Tab.Pane>
                        <Tab.Pane eventKey="fourth">
                          <p>Escriba una función que verifique si un elemento aparece en una lista.</p>
                        </Tab.Pane>
                        <Tab.Pane eventKey="fifth">
                          <p>Escriba una función que devuelva los elementos en posiciones impares en una lista.</p>
                        </Tab.Pane>
                        <Tab.Pane eventKey="sixth">
                          <p>Escriba una función que calcule el total acumulado de una lista.</p>
                        </Tab.Pane>
                        <Tab.Pane eventKey="seventh">
                          <p>Escribe una función que compruebe si una cadena es un palíndromo.</p>
                        </Tab.Pane>
                        <Tab.Pane eventKey="eighth">
                          <p>Escriba tres funciones que calculen la suma de los números en una lista: usando un bucle for, un bucle while y una recursión. (Sujeto a la disponibilidad de estas construcciones en el idioma de su elección).</p>
                        </Tab.Pane>
                        <Tab.Pane eventKey="nineth">
                          <p>Escriba una función on_all que aplique una función a cada elemento de una lista. Úsalo para imprimir los primeros veinte cuadrados perfectos. Los cuadrados perfectos se pueden encontrar multiplicando cada número natural consigo mismo. Los primeros cuadrados perfectos son 1 * 1 = 1, 2 * 2 = 4, 3 * 3 = 9, 4 * 4 = 16. Doce, por ejemplo, no es un cuadrado perfecto porque no hay un número natural m, por lo que m * m = 12. (Esta pregunta es complicada si su lenguaje de programación hace que sea difícil pasar las funciones como argumentos).</p>
                        </Tab.Pane>
                        <Tab.Pane eventKey="tenth">
                          <p>Escribe una función que concatene dos listas. [a, b, c], [1,2,3] → [a, b, c, 1,2,3]</p>
                        </Tab.Pane>
                      </Tab.Content>
                    </Col>
                  </Row>
                </Tab.Container>
              </Container>
            </React.Fragment>
        )
    }
}
