import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import Home from './Home'
import EjElementales from './EjElementales'
import EjCad from './EjCad'
import EjIntermedios from './EjIntermedios'
import EjAvanzados from './EjAvanzados'
import Tetris from './Gtetris'
import NoMatch from './NoMatch'

function App() {
  return (
    <React.Fragment>
        <Router>
          <Switch>
            <Route exact path='/' component={Home}/>
            <Route path='/elemental' component={EjElementales}/>
            <Route path='/cadena' component={EjCad}/>
            <Route path='/intermedio' component={EjIntermedios}/>
            <Route path='/avanzado' component={EjAvanzados}/>
            <Route path='/tetris' component={Tetris}/>
            <Route component={NoMatch}/>
          </Switch>
        </Router>
    </React.Fragment>
  );
}

export default App
