import React from 'react'
import BarraNav from './components/barra-nav/BarraNav'
import Jumbotron from 'react-bootstrap/Jumbotron'
import Container from 'react-bootstrap/Container'
import Card from 'react-bootstrap/Card'
import CardDeck from 'react-bootstrap/CardDeck'
import ReactLogo from './images/react.svg'
import AngularLogo from './images/angular.svg'
import VueLogo from './images/vue.svg'
import './Home.css'

export default function Home() {
    return (
        <React.Fragment>
            <BarraNav />
            <Jumbotron className='Jumbo' fluid>
                <Container>
                    <h1>Aplicación web con React</h1>
                    <p>
                    Este es una aplicación web con HTML5, CSS3, Bootstrap 4 y React.
                    </p>
                </Container>
            </Jumbotron>
            <Container>
                <CardDeck style={{ padding: "15px" }}>
                    <Card className="border-info">
                        <Card.Img variant="top" src={ReactLogo} className="ImageCard" />
                        <Card.Body>
                        <Card.Title className="text-info text-center">REACT</Card.Title>
                        <Card.Text className="text-card">
                            React te ayuda a crear interfaces de usuario interactivas de forma sencilla.
                            Diseña vistas simples para cada estado en tu aplicación, y React se encargará
                            de actualizar y renderizar de manera eficiente los componentes correctos cuando
                            los datos cambien.
                        </Card.Text>
                        </Card.Body>
                    </Card>
                    <Card className="border-danger">
                        <Card.Img variant="top" src={AngularLogo} className="ImageCard" />
                        <Card.Body>
                        <Card.Title className="text-danger text-center">ANGULAR</Card.Title>
                        <Card.Text className="text-card">
                            HTML es ideal para declarar documentos estáticos, pero falla cuando intentamos
                            usarlo para declarar vistas dinámicas en aplicaciones web. AngularJS le permite
                            ampliar el vocabulario HTML para su aplicación. El entorno resultante es
                            extraordinariamente expresivo, legible y rápido de desarrollar.
                        </Card.Text>
                        </Card.Body>
                    </Card>
                    <Card className="border-success">
                        <Card.Img variant="top" src={VueLogo} className="ImageCard" />
                        <Card.Body>
                        <Card.Title className="text-success text-center">VUE</Card.Title>
                        <Card.Text className="text-card">
                            La biblioteca principal se centra solo en la capa de vista y es fácil
                            de recoger e integrar con otras bibliotecas o proyectos existentes. Por otro lado,
                            Vue también es perfectamente capaz de impulsar aplicaciones sofisticadas de una
                            sola página cuando se usa en combinación con herramientas modernas y bibliotecas de soporte.
                        </Card.Text>
                        </Card.Body>
                    </Card>
                </CardDeck>
            </Container>
        </React.Fragment>
    )
}