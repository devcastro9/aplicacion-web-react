import React from 'react'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import NavDropdown from 'react-bootstrap/NavDropdown'
import Image from 'react-bootstrap/Image'
import logo from '../../images/logo.svg'
import './logo-style.css'

function BarraNav() {
    return (
        <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
            <Navbar.Brand href="/"><Image src={logo} className="App-logo" /></Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
                <Nav className="mr-auto">
                    <Nav.Link href="/features">Link 1</Nav.Link>
                    <Nav.Link href="/pricing">Link 2</Nav.Link>
                    <NavDropdown title="Ejercicios" id="collasible-nav-dropdown">
                        <NavDropdown.Item href="/elemental">Ejercicios Elementales</NavDropdown.Item>
                        <NavDropdown.Item href="/cadena">Listas, cadenas y textos.</NavDropdown.Item>
                        <NavDropdown.Item href="/intermedio">Ejercicios Intermedios</NavDropdown.Item>
                        <NavDropdown.Item href="/avanzado">Ejercicios Avanzados</NavDropdown.Item>
                    </NavDropdown>
                </Nav>
                <Nav>
                    <Nav.Link href="/deets">Link 3</Nav.Link>
                    <Nav.Link href="/tetris">Tetris</Nav.Link>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}

export default BarraNav
