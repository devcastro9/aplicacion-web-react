import React, { useState } from 'react'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Modal from 'react-bootstrap/Modal'

export default function Ejer5() {
    const [show, setShow] = useState(false);
    const [numero, setNumero] = useState(0);
    const [mensaje, setMensaje] = useState("");
    function handleChange(event) {
        setNumero(event.target.value);
    }
    function handleClose() {
        setNumero(0);
        setShow(false);
    }
    function handleShow() {
        function Generar(n=1) {
            let arr = Array.from({length: n}, (v,k) => (k+1) % 3 === 0? k+1 : 0).filter(num => num > 0);
            return arr.join(' + ').concat(' = ') + arr.length*(3 + arr[arr.length-1])/2;    // Logica de solucion
        }
        setShow(true);
        setMensaje(numero > 2? Generar(numero): "El número no es válido.");
    }
    
    return (
        <React.Fragment>
            <Form>
                <Form.Group controlId="id-nombre">
                    <Form.Label>Número</Form.Label>
                    <Form.Control type="number" placeholder="Ingrese un número..." value={numero} onChange={handleChange} />
                    <Form.Text className="text-muted">Realizado con React-Hooks</Form.Text>
                </Form.Group>
                <Button variant="success" onClick={handleShow}>
                    EJECUTAR
                </Button>
            </Form>
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Ejercicio 5</Modal.Title>
                </Modal.Header>
                <Modal.Body>{mensaje}</Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Cerrar
                    </Button>
                </Modal.Footer>
            </Modal>
        </React.Fragment>
    )
}
