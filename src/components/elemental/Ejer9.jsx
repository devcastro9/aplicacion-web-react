import React, { useState } from 'react'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'

export default function Ejer9() {
    const [primos, setPrimos] = useState("");
    function Generar() {
        function esPrimo(numero = 0) {
            // Retorna true si es primo, en otro caso false.
            let k = 2;
            while (k < Math.floor(numero/2)+1 ) {
                if (numero % k === 0) {
                    return false;
                }
                k++;
            }
            return true;
        }
        return Array.from({length: 1000}, (v,k) => esPrimo(k+1)? k+1 : 0).filter(num => num > 0).join(', ').concat('.');    // Logica de solucion
    }

    return (
        <React.Fragment>
            <Form>
                <Button variant="success" onClick={() => setPrimos(Generar())}>
                    EJECUTAR
                </Button>
                &nbsp;&nbsp;&nbsp;
                <Button variant="danger" onClick={() => setPrimos("")}>
                    BORRAR
                </Button>
                <Form.Text className="text-muted">Realizado con React-Hooks</Form.Text>
            </Form>
            <br/>
            <p>{primos}</p>
        </React.Fragment>
    )
}
