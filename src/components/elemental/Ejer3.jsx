import React, { useState } from 'react'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Modal from 'react-bootstrap/Modal'

export default function Ejer3() {
    const [show, setShow] = useState(false);
    const [nombre, setNombre] = useState("");
    const [mensaje, setMensaje] = useState("");
    function handleChange(event) {
        setNombre(event.target.value);
    }
    function handleClose() {
        setNombre("");
        setShow(false);
    }
    function handleShow() {
        setShow(true);
        setMensaje(nombre.toLowerCase() === "alicia" || nombre.toLowerCase() === "roberto" ? "¡Hola " + nombre + "!" : "No válido. Válido solo para usuarios: Alicia y Roberto."); // Logica de solucion
    }
    return (
        <React.Fragment>
            <Form>
                <Form.Group controlId="id-nombre">
                    <Form.Label>Nombre</Form.Label>
                    <Form.Control type="text" placeholder="Ingrese su nombre..." value={nombre} onChange={handleChange} />
                    <Form.Text className="text-muted">Realizado con React-Hooks</Form.Text>
                </Form.Group>
                <Button variant="success" onClick={handleShow}>
                    EJECUTAR
                </Button>
            </Form>
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Ejercicio 3</Modal.Title>
                </Modal.Header>
                <Modal.Body>{mensaje}</Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Cerrar
                    </Button>
                </Modal.Footer>
            </Modal>
        </React.Fragment>
    )
}
