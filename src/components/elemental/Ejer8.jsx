import React, { useState } from 'react'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Modal from 'react-bootstrap/Modal'

export default function Ejer8() {
    const [show, setShow] = useState(false);
    const [numero, setNumero] = useState(0);
    const [mensaje, setMensaje] = useState("");
    function handleChange(event) {
        setNumero(event.target.value);
    }
    function handleClose() {
        setNumero(0);
        setShow(false);
    }
    function handleShow() {
        function Generar(n=1) {
            const arr = Array.from({length: 12}, (v,k) => k+1);
            return arr.map((item) => <li>{item + ' x ' + n + " = " + item*n}</li> );     // Logica de solucion
        }
        setShow(true);
        setMensaje(numero > 0? Generar(numero): "El número no es válido.");
    }
    
    return (
        <React.Fragment>
            <Form>
                <Form.Group controlId="id-nombre">
                    <Form.Label>Número</Form.Label>
                    <Form.Control type="number" placeholder="Ingrese un número..." value={numero} onChange={handleChange} />
                    <Form.Text className="text-muted">Realizado con React-Hooks</Form.Text>
                </Form.Group>
                <Button variant="success" onClick={handleShow}>
                    EJECUTAR
                </Button>
            </Form>
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Ejercicio 8</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h4>Tabla de multiplicar del número: {numero}</h4>
                    <br/>
                    <ul>{mensaje}</ul>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Cerrar
                    </Button>
                </Modal.Footer>
            </Modal>
        </React.Fragment>
    )
}
