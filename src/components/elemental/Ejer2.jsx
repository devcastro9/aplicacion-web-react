import React, { Component } from 'react'
import Button from 'react-bootstrap/Button'
import Modal from 'react-bootstrap/Modal'
import Form from 'react-bootstrap/Form'

export default class Ejer2 extends Component {
    constructor(props) {
        super(props)
        this.state = {
            show: false,
            nombre: "",
            message: ""
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleShow = this.handleShow.bind(this);
    }

    handleChange(event) {
        this.setState({
            nombre: event.target.value
        });
    }

    handleClose() {
        this.setState({
            show: false,
            nombre: "",
        });
    }

    handleShow() {
        this.setState({
            show: true,
            message: this.state.nombre.length > 2 ? "¡Hola " + this.state.nombre + "!" : "No válido" // Logica de solucion
        });
    }

    render() {
        return (
            <React.Fragment>
                <Form>
                    <Form.Group controlId="id-nombre">
                        <Form.Label>Nombre</Form.Label>
                        <Form.Control type="text" placeholder="Ingrese su nombre..." value={this.state.nombre} onChange={this.handleChange} />
                        <Form.Text className="text-muted">Realizado con React-Components</Form.Text>
                    </Form.Group>
                    <Button variant="success" onClick={this.handleShow}>
                        EJECUTAR
                    </Button>
                </Form>
                <Modal show={this.state.show} onHide={this.handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>Ejercicio 2</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>{this.state.message}</Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={this.handleClose}>
                            Cerrar
                        </Button>
                    </Modal.Footer>
                </Modal>
            </React.Fragment>
        )
    }
}
