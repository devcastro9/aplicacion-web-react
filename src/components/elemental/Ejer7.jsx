import React, { useState } from 'react'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Modal from 'react-bootstrap/Modal'

export default function Ejer7() {
    const [show, setShow] = useState(false);
    const [numero, setNumero] = useState(0);
    const [mensaje, setMensaje] = useState("");
    function handleChange(event) {
        setNumero(event.target.value);
    }
    function handleClose() {
        setNumero(0);
        setShow(false);
    }
    function handleShow(opc = 0) {
        function Generar(n = 1, opc_sum_prod = 0) {
            let arr = Array.from({length: n}, (v,k) => k+1 );
            return opc_sum_prod === 0? arr.join(' + ').concat(' = ') + arr.reduce((acumulador, val) => acumulador + val) : arr.join(' * ').concat(' = ') + arr.reduce((acumulador, val) => acumulador * val) ;    // Logica de solucion
        }
        setShow(true);
        setMensaje(numero > 1? Generar(numero, opc): "El número no es válido.");
    }
    
    return (
        <React.Fragment>
            <Form>
                <Form.Group controlId="id-nombre">
                    <Form.Label>Número</Form.Label>
                    <Form.Control type="number" placeholder="Ingrese un número..." value={numero} onChange={handleChange} />
                    <Form.Text className="text-muted">Realizado con React-Hooks</Form.Text>
                </Form.Group>
                <Button variant="success" onClick={() => handleShow(0)}>
                    EJECUTAR SUMA
                </Button>
                &nbsp;&nbsp;&nbsp;
                <Button variant="danger" onClick={() => handleShow(1)}>
                    EJECUTAR PRODUCTO
                </Button>
            </Form>
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Ejercicio 7</Modal.Title>
                </Modal.Header>
                <Modal.Body>{mensaje}</Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Cerrar
                    </Button>
                </Modal.Footer>
            </Modal>
        </React.Fragment>
    )
}
