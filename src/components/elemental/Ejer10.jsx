import React, { useState } from 'react'
import Form from 'react-bootstrap/Form'
import Container from 'react-bootstrap/Container'
import Button from 'react-bootstrap/Button'
import CardDeck from 'react-bootstrap/CardDeck'
import Card from 'react-bootstrap/Card'

export default function Ejer10() {
    let [numero, setNumero] = useState(0);
    let [intentos, setIntentos] = useState(-1);
    let [mensaje, setMensaje] = useState("¡Haga su primer intento!");
    let [num_secreto, setSecreto] = useState(0);
    
    function handleChange(event) {
        setNumero(event.target.value);
    }
    function Generar() {
        if (intentos < 0) {
            setSecreto(Math.floor(Math.random()*100));
            setMensaje("Se ha generado el número secreto!");
        }
        else {
            setMensaje(numero > num_secreto? "El número " + numero + " es mayor al número secreto! " : (numero < num_secreto ? "El número " + numero + " es menor al número secreto!" : "Felicidades hallo el número secreto " + num_secreto +" !" ) );
        }
        setIntentos(intentos + 1);
    }
    function Reiniciar() {
        setNumero(0);
        setIntentos(-1);
        setSecreto(0);
        setMensaje("¡Haga su primer intento!");
    }

    return (
        <React.Fragment>
            <Form>
                <Form.Group controlId="id-10">
                    <Form.Label>Ingrese un número:</Form.Label>
                    <Form.Control type="number" placeholder="Ingrese un número..." value={numero} onChange={handleChange} />
                    <Form.Text className="text-muted">Realizado con React-Hooks</Form.Text>
                </Form.Group>
                <Button variant="danger" onClick={Reiniciar}>
                    NUEVO JUEGO
                </Button>
                &nbsp;&nbsp;&nbsp;
                <Button variant={intentos > -1? "success" : "warning"} onClick={Generar}>
                    {intentos > -1? "JUGAR" : "GENERAR NÚMERO SECRETO"}
                </Button>
                {/* <p>Número secreto: {num_secreto}</p> */}
            </Form>
            <Container>
                <CardDeck style={{ padding: "15px" }}>
                    <Card className="border-info">
                        <Card.Body>
                        <Card.Title className="text-info">Estado del juego</Card.Title>
                        <Card.Text className="text-card text-center">
                            <p className="display-3">{numero}</p>
                            <p>{mensaje}</p>
                        </Card.Text>
                        </Card.Body>
                    </Card>
                    <Card className="border-success">
                        <Card.Body>
                        <Card.Title className="text-success">Nro. Intentos:</Card.Title>
                        <Card.Text className="text-card text-center">
                            <p className={intentos > 0? "display-1" : "h1"}>{intentos > 0? intentos : (intentos < 0? "¡Aun no inicio el juego!" : "¡Haga su primer intento!")}</p>
                        </Card.Text>
                        </Card.Body>
                    </Card>
                </CardDeck>
            </Container>
        </React.Fragment>
    )
}
