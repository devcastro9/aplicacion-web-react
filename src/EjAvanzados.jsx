import React, { Component } from 'react'
import BarraNav from './components/barra-nav/BarraNav'

export default class EjAvanzados extends Component {
    render() {
        return (
            <React.Fragment>
                <BarraNav />
                <h1>Ejercicios EjAvanzados</h1>
            </React.Fragment>
        )
    }
}