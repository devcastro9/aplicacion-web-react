import React, { Component } from 'react'
import BarraNav from './components/barra-nav/BarraNav'
import Container from 'react-bootstrap/Container'
import Nav from 'react-bootstrap/Nav'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Tab from 'react-bootstrap/Tab'
import Ejer1 from './components/elemental/Ejer1'
import Ejer2 from './components/elemental/Ejer2'
import Ejer3 from './components/elemental/Ejer3'
import Ejer4 from './components/elemental/Ejer4'
import Ejer5 from './components/elemental/Ejer5'
import Ejer6 from './components/elemental/Ejer6'
import Ejer7 from './components/elemental/Ejer7'
import Ejer8 from './components/elemental/Ejer8'
import Ejer9 from './components/elemental/Ejer9'
import Ejer10 from './components/elemental/Ejer10'

export default class EjElementales extends Component {
    render() {
        return (
            <React.Fragment>
              <BarraNav />
              <Container>
                <h1 className="text-center text-primary font-weight-bold p-3">Ejercicios Elementales</h1>
                <Tab.Container id="left-tabs-example" defaultActiveKey="first">
                  <Row>
                    <Col sm={3}>
                      <Nav variant="pills" className="flex-column">
                        <Nav.Item>
                          <Nav.Link eventKey="first">Ejercicio 1</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                          <Nav.Link eventKey="second">Ejercicio 2</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                          <Nav.Link eventKey="third">Ejercicio 3</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                          <Nav.Link eventKey="fourth">Ejercicio 4</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                          <Nav.Link eventKey="fifth">Ejercicio 5</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                          <Nav.Link eventKey="sixth">Ejercicio 6</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                          <Nav.Link eventKey="seventh">Ejercicio 7</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                          <Nav.Link eventKey="eighth">Ejercicio 8</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                          <Nav.Link eventKey="nineth">Ejercicio 9</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                          <Nav.Link eventKey="tenth">Ejercicio 10</Nav.Link>
                        </Nav.Item>
                      </Nav>
                    </Col>
                    <Col sm={9}>
                      <Tab.Content>
                        <Tab.Pane eventKey="first">
                          <p>Escriba un programa que imprima 'Hola Mundo' en la pantalla.</p>
                          <Ejer1 />
                        </Tab.Pane>
                        <Tab.Pane eventKey="second">
                          <p>Escriba un programa que le pida al usuario su nombre y lo salude con su nombre.</p>
                          <Ejer2 />
                        </Tab.Pane>
                        <Tab.Pane eventKey="third">
                          <p>Modifique el programa anterior de modo que solo los usuarios Alicia y Roberto sean recibidos con sus nombres.</p>
                          <Ejer3 />
                        </Tab.Pane>
                        <Tab.Pane eventKey="fourth">
                          <p>Escriba un programa que solicite al usuario un número 'n' e imprima la suma de los números 1 a n.</p>
                          <Ejer4 />
                        </Tab.Pane>
                        <Tab.Pane eventKey="fifth">
                          <p>Modifique el programa anterior de modo que solo se consideren múltiplos de tres en la suma, p. Ej. 3, 6, 9, 12, 15, ...</p>
                          < Ejer5 />
                        </Tab.Pane>
                        <Tab.Pane eventKey="sixth">
                          <p>Modifique el programa anterior de modo que solo se consideren múltiplos de tres o cinco en la suma, p. Ej. 3, 5, 6, 9, 10, 12, 15, ...</p>
                          <Ejer6 />
                        </Tab.Pane>
                        <Tab.Pane eventKey="seventh">
                          <p>Escriba un programa que le pida al usuario un número n y le dé la posibilidad de elegir entre calcular la suma y calcular el producto de 1, ..., n.</p>
                          <Ejer7 />
                        </Tab.Pane>
                        <Tab.Pane eventKey="eighth">
                          <p>Escribe un programa que imprima una tabla de multiplicar para los números hasta 12.</p>
                          <Ejer8 />
                        </Tab.Pane>
                        <Tab.Pane eventKey="nineth">
                          <p>Escribe un programa que imprima todos los números primos. (Nota: si su lenguaje de programación no admite números de tamaño arbitrario, también está bien imprimir todos los números primos hasta el número más grande que pueda representar fácilmente).</p>
                          <Ejer9 />
                        </Tab.Pane>
                        <Tab.Pane eventKey="tenth">
                          <p>Escribe un juego de adivinanzas donde el usuario tenga que adivinar un número secreto. Después de cada conjetura, el programa le dice al usuario si su número era demasiado grande o demasiado pequeño. Al final se debe imprimir el número de intentos necesarios.</p>
                          <Ejer10 />
                        </Tab.Pane>
                      </Tab.Content>
                    </Col>
                  </Row>
                </Tab.Container>
              </Container>
            </React.Fragment>
        )
    }
}
